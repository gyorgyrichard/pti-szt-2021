package hu.depti;

public class Calculator {

  public int add(int a, int b) {
    return a + b;
  }

  public int substract(int a, int b) {
    return a - b;
  }

  public int multiply(int a, int b) {
    return a * b;
  }

  public int square(int a) {
    return a * a;
  }

  public int squareAplusB(int a,int b){return square(a) + 2*a*b + square(b);}

  public int squareABMultiply(int a, int b){return square(multiply(a,b));}

  public int divide(int a, int b) throws IllegalArgumentException {
    if (b == 0) {
      throw new IllegalArgumentException("Nullával való osztás nem értelmezett");
    }
    return a / b;
  }
}
